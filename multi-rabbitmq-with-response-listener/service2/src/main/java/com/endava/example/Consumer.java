package com.endava.example;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;

@Component
@RequiredArgsConstructor
public class Consumer {

    private static Logger logger = LoggerFactory.getLogger(Consumer.class);

    private final RabbitTemplate remoteRabbitTemplate;
    @Value("${respond.on.new.connection}")
    private boolean respondOnNewConnection;
    @Value("${rabbitmq.consumer.local.queue.durable}")
    private boolean isDurable;

    @RabbitListener(
            containerFactory = "localRabbitListenerContainerFactory",
            admin = "localAmqpAdmin",
            queues = SpringRabbitMQConsumerApp.LOCAL_QUEUE
    )
    public void listen(Message message) {
        Instant endTimeReceiveInReceiver = Instant.now();
        message.getMessageProperties().setHeader("endTimeReceiveInReceiver", endTimeReceiveInReceiver);
        Instant startTimeSendToReceiver = Instant.parse(message.getMessageProperties().getHeader("startTimeSendToReceiver"));
        logger.debug("Received message from sender in {}", Duration.between(startTimeSendToReceiver, endTimeReceiveInReceiver));

        String exchange = message.getMessageProperties().getHeader("respondToExchange");
        String routingKey = message.getMessageProperties().getHeader("respondToRoutingKey");

        if (respondOnNewConnection) {
            String host = message.getMessageProperties().getHeader("respondToHost");
            int port = message.getMessageProperties().getHeader("respondToPort");
            sendResponseWithNewConnection(host, port, exchange, routingKey, message);
        } else {
            sendResponse(remoteRabbitTemplate, exchange, routingKey, message);
        }
    }

    private void sendResponseWithNewConnection(String host, int port, String exchange, String routingKey, Message message) {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host, port);
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);

        try {
            sendResponse(rabbitTemplate, exchange, routingKey, message);
        } finally {
            rabbitTemplate.stop();
            connectionFactory.destroy();
        }
    }

    private void sendResponse(RabbitTemplate rabbitTemplate, String exchange, String routingKey, Message message) {
        message.getMessageProperties().setHeader("startTimeSendToSender", Instant.now());
        message.getMessageProperties().setDeliveryMode(isDurable ? MessageDeliveryMode.PERSISTENT : MessageDeliveryMode.NON_PERSISTENT);
        rabbitTemplate.convertAndSend(exchange, routingKey, message);
    }
}
