package com.endava.example;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import static com.endava.example.SpringRabbitMQProducerApplication.*;

@Service
@RequiredArgsConstructor
public class Producer {

    private static Logger logger = LoggerFactory.getLogger(Producer.class);

    private final RabbitTemplate remoteRabbitTemplate;

    @Value("#{'${rabbitmq.producer.message-content}'.getBytes()}")
    private byte[] messageContent;
    @Value("${rabbitmq.producer.local.host}")
    private String consumerResponseHost;
    @Value("${rabbitmq.producer.local.port}")
    private int consumerResponsePort;
    @Value("${rabbitmq.producer.local.queue.durable}")
    private boolean isDurable;

    private Map<String, Object> headers = new HashMap<>();

    @PostConstruct
    private void init() {
        headers.put("respondToHost", consumerResponseHost);
        headers.put("respondToPort", consumerResponsePort);
        headers.put("respondToExchange", LOCAL_EXCHANGE);
        headers.put("respondToRoutingKey", LOCAL_ROUTING_KEY);
    }

    public void sendDirectExchangeMessage() {
        logger.debug("[Direct Exchange] Sending message");
        remoteRabbitTemplate.convertAndSend(
                REMOTE_EXCHANGE,
                REMOTE_ROUTING_KEY,
                new Message(
                        messageContent,
                        getMessageProps()
                                .setHeader("final", false)
                                .build()
                ));
    }

    public void sendFinalMessage(int benchmarkId, boolean isLastRepeat) {
        logger.debug("Sending message final message");
        remoteRabbitTemplate.convertAndSend(
                REMOTE_EXCHANGE,
                REMOTE_ROUTING_KEY,
                new Message(
                        messageContent,
                        getMessageProps()
                                .setHeader("final", true)
                                .setHeader("lastRepeat", isLastRepeat)
                                .setHeader("benchmarkIterationId", benchmarkId)
                                .build())
        );
    }

    private MessageBuilderSupport<MessageProperties> getMessageProps() {
        return MessagePropertiesBuilder.newInstance()
                .setContentType(MessageProperties.CONTENT_TYPE_JSON)
                .copyHeaders(headers)
                .setHeader("startTimeSendToReceiver", Instant.now())
                .setDeliveryMode(isDurable ? MessageDeliveryMode.PERSISTENT : MessageDeliveryMode.NON_PERSISTENT);
    }
}
