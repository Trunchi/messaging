package com.endava.example;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(exclude = RabbitAutoConfiguration.class)
@EnableScheduling
@EnableRabbit
@EnableAsync
public class SpringRabbitMQProducerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringRabbitMQProducerApplication.class);
    }

    public static final String REMOTE_EXCHANGE = "e2";
    public static final String REMOTE_ROUTING_KEY = "r2";
    public static final String LOCAL_EXCHANGE = "e1";
    public static final String LOCAL_ROUTING_KEY = "r1";
    public static final String LOCAL_QUEUE = "q1";

    @Bean
    public ConnectionFactory remoteConnectionFactory(
            @Value("${rabbitmq.producer.remote.host}") String host,
            @Value("${rabbitmq.producer.remote.port}") int port
    ) {
        return new CachingConnectionFactory(host, port);
    }

    @Bean
    public RabbitTemplate remoteRabbitTemplate(ConnectionFactory remoteConnectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(remoteConnectionFactory);
        rabbitTemplate.containerAckMode(AcknowledgeMode.NONE);
        return rabbitTemplate;
    }

    // for response listener:
    @Bean
    public ConnectionFactory localConnectionFactory(
            @Value("${rabbitmq.producer.local.host}") String host,
            @Value("${rabbitmq.producer.local.port}") int port
    ) {
        return new CachingConnectionFactory(host, port);
    }

    @Bean
    public AmqpAdmin localAmqpAdmin(ConnectionFactory localConnectionFactory) {
        return new RabbitAdmin(localConnectionFactory);
    }

    @Bean
    public DirectExchange localExchange(
            AmqpAdmin localAmqpAdmin
    ) {
        DirectExchange exchange = new DirectExchange(LOCAL_EXCHANGE);
        localAmqpAdmin.declareExchange(exchange);
        Queue queue1 = new Queue(LOCAL_QUEUE);
        localAmqpAdmin.declareQueue(queue1);
        localAmqpAdmin.declareBinding(BindingBuilder.bind(queue1)
                .to(exchange).with(LOCAL_ROUTING_KEY));

        return exchange;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory localRabbitListenerContainerFactory(ConnectionFactory localConnectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(localConnectionFactory);
        return factory;
    }
}