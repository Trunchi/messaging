package com.endava.example.h2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.Instant;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Benchmark {
    @Id
    @GeneratedValue
    private int id;
    private Instant startTime;
    private Instant endTime;
    private int messageCount;
    private int iterations;

    public Benchmark(Instant startTime, int messageCount, int iterations) {
        this.startTime = startTime;
        this.messageCount = messageCount;
        this.iterations = iterations;
    }
}
