package com.endava.example;

import com.endava.example.h2.Benchmark;
import com.endava.example.h2.BenchmarkIteration;
import com.endava.example.h2.BenchmarkIterationRepository;
import com.endava.example.h2.BenchmarkRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BenchmarkRunner {

    private static Logger logger = LoggerFactory.getLogger(BenchmarkRunner.class);

    private final BenchmarkRepository benchmarkRepository;
    private final BenchmarkIterationRepository benchmarkIterationRepository;

    private final Producer rabbitMQProducer;

    @Async
    public void start(int iterations, int messageCount) {
        logger.info("\nStarting benchmark: \nIterations: {}\nNumber of messages in each iteration: {}", iterations, messageCount);
        Benchmark benchmark = new Benchmark(Instant.now(), messageCount, iterations);
        benchmarkRepository.save(benchmark);
        run(1, benchmark);
    }

    public void run(int iterationNumber, Benchmark benchmark) {
        BenchmarkIteration benchmarkIteration = new BenchmarkIteration(iterationNumber, Instant.now(), benchmark);
        benchmarkIterationRepository.save(benchmarkIteration);
        logger.info("Starting to send {} messages", benchmarkIteration.getBenchmark().getMessageCount());

        for (int i = 1; i < benchmark.getMessageCount(); i++) {
            rabbitMQProducer.sendDirectExchangeMessage();
        }
        rabbitMQProducer.sendFinalMessage(
                benchmarkIteration.getId(),
                iterationNumber == benchmarkIteration.getBenchmark().getIterations()
        );

        benchmarkIteration.setSendEndTime(Instant.now());
        benchmarkIterationRepository.save(benchmarkIteration);
        logger.info("Sending {} messages finished in {}", benchmark.getMessageCount(),
                Duration.between(benchmarkIteration.getStartTime(), benchmarkIteration.getSendEndTime()));
    }

    public void finishIteration(Instant endTimeReceiveInSender, Integer benchmarkIterationId, boolean isLastRepeat) {
        BenchmarkIteration benchmarkIteration = benchmarkIterationRepository
                .findById(benchmarkIterationId)
                .orElseThrow();
        benchmarkIteration.setEndTime(endTimeReceiveInSender);
        benchmarkIterationRepository.save(benchmarkIteration);

        logger.info("Iteration finished in {}", Duration.between(benchmarkIteration.getStartTime(), benchmarkIteration.getEndTime()));

        if (!isLastRepeat) {
            run(benchmarkIteration.getIterationNumber() + 1, benchmarkIteration.getBenchmark());
        } else {
            logger.info("All iterations finished.");
            benchmarkTime(benchmarkIteration);
        }
    }

    private void benchmarkTime(BenchmarkIteration benchmarkIteration) {
        List<BenchmarkIteration> allByBenchmarkId = benchmarkIterationRepository.findAllByBenchmarkId(benchmarkIteration.getBenchmark().getId());

        long avgSendNanos = allByBenchmarkId.stream()
                .map(bi -> Duration.between(bi.getStartTime(), bi.getSendEndTime()).toNanos())
                .reduce(0L, Long::sum)
                / allByBenchmarkId.size();
        long avgTotalNanos = allByBenchmarkId.stream()
                .map(bi -> Duration.between(bi.getStartTime(), bi.getEndTime()).toNanos())
                .reduce(0L, Long::sum)
                / allByBenchmarkId.size();

        logger.info(
                "\nSent {} messages, {} times.\nAverage send time: {} ({} ns)\nAverage iteration time: {} ({} ns)\nMessage send rate: {}\nMessage total rate: {}",
                benchmarkIteration.getBenchmark().getMessageCount(),
                benchmarkIteration.getBenchmark().getIterations(),
                Duration.ofNanos(avgSendNanos),
                avgSendNanos,
                Duration.ofNanos(avgTotalNanos),
                avgTotalNanos,
                Duration.ofNanos(avgSendNanos / benchmarkIteration.getBenchmark().getMessageCount()),
                Duration.ofNanos(avgTotalNanos / benchmarkIteration.getBenchmark().getMessageCount())
        );
    }
}
