package com.endava.example.h2;

import org.springframework.data.repository.CrudRepository;

public interface BenchmarkRepository extends CrudRepository<Benchmark, Integer> {
}
