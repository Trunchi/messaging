package com.endava.example;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class BenchmarkController {
    private final BenchmarkRunner benchmarkRunner;

    @GetMapping("/benchmark/iterations/{iterations}/messagecount/{messageCount}")
    public void benchmark(@PathVariable int iterations, @PathVariable int messageCount) {
        benchmarkRunner.start(iterations, messageCount);
    }
}
