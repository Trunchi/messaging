package com.endava.example;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;

@Service
@RequiredArgsConstructor
public class ResponseListener {

    private static Logger logger = LoggerFactory.getLogger(ResponseListener.class);

    private final BenchmarkRunner benchmarkRunner;

    @RabbitListener(
            containerFactory = "localRabbitListenerContainerFactory",
            queues = SpringRabbitMQProducerApplication.LOCAL_QUEUE,
            admin = "localAmqpAdmin"
    )
    public void onMessage(Message message) {
        Instant endTimeReceiveInSender = Instant.now();
        Instant startTimeSendToReceiver = Instant.parse(message.getMessageProperties().getHeader("startTimeSendToReceiver"));
        logger.debug("Round trip duration {}", Duration.between(startTimeSendToReceiver, endTimeReceiveInSender));
        Boolean isFinal = message.getMessageProperties().getHeader("final");
        if (Boolean.TRUE.equals(isFinal)) {
            logger.info("Sending messages finished.");
            benchmarkRunner.finishIteration(
                    endTimeReceiveInSender,
                    message.getMessageProperties().getHeader("benchmarkIterationId"),
                    message.getMessageProperties().getHeader("lastRepeat")
            );
        }
    }


}
