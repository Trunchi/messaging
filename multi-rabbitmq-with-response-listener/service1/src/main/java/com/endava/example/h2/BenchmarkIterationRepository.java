package com.endava.example.h2;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BenchmarkIterationRepository extends CrudRepository<BenchmarkIteration, Integer> {

    @Query("SELECT bi FROM BenchmarkIteration bi WHERE bi.benchmark.id = ?1")
    List<BenchmarkIteration> findAllByBenchmarkId(int benchmarkId);
}
