package com.endava.example.zeromq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;
import org.zeromq.ZMQException;
import zmq.ZError;

@SpringBootApplication
public class ZeroMQReceiver {
    public static void main(String[] args) {
        SpringApplication.run(ZeroMQReceiver.class, args);

        try {
            ZMQ.Context context = ZMQ.context(1);
//            ZMQ.Socket socket = context.socket(SocketType.PULL);
            ZMQ.Socket socket = context.socket(SocketType.SUB);
            socket.connect("tcp://localhost:5555");
            socket.subscribe("");
            System.out.println("Receiver - Listener created");

            while (true) {
                String message;
                try {
                    message = socket.recvStr();
                } catch (ZMQException e) {
                    if (e.getErrorCode() == ZError.ETERM)
                        break;
                    e.printStackTrace();
                    break;
                }
                if (!message.isEmpty())
                    System.out.println("Received message: " + message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(">> Exited");
    }
}
