package com.endava.example.zeromq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.zeromq.SocketType;
import org.zeromq.ZMQ;

@SpringBootApplication
@EnableScheduling
public class ZeroMQSender {
    public static void main(String[] args) {
        SpringApplication.run(ZeroMQSender.class, args);
    }

    @Bean
    ZMQ.Socket socket() {
        try {
            ZMQ.Context context = ZMQ.context(1);
//            ZMQ.Socket socket = context.socket(SocketType.PUSH);
            ZMQ.Socket socket = context.socket(SocketType.PUB);
            socket.bind("tcp://localhost:5555");
            System.out.println("Sender - Socket created");
            return socket;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        return null;
    }
}