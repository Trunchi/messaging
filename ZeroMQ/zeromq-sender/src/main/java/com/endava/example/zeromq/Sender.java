package com.endava.example.zeromq;

import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.zeromq.ZMQ;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

@Component
@AllArgsConstructor
public class Sender {
    private static final List<String> values = Arrays.asList("one", "two", "three", "four");
    private static final Random rnd = new Random();
    private ZMQ.Socket socket;

    @Scheduled(fixedRate = 3_000)
    void send() {
        String message = "Message " + values.get(rnd.nextInt(values.size()));
        System.out.println("Sending messages x10 " + message);

        IntStream.range(0, 10)
                .forEach(i -> socket.send(message + " #" + i));
    }
}
