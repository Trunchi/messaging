package com.endava.example.h2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BenchmarkIteration {
    @Id
    @GeneratedValue
    private int id;
    private int iterationNumber;
    private Instant startTime;
    private Instant sendEndTime;
    private Instant endTime;
    @ManyToOne
    private Benchmark benchmark;

    public BenchmarkIteration(int iterationNumber, Instant startTime, Benchmark benchmark) {
        this.iterationNumber = iterationNumber;
        this.startTime = startTime;
        this.benchmark = benchmark;
    }
}
