package com.endava.example;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.jms.DeliveryMode;
import javax.jms.Queue;

@SpringBootApplication
@EnableJms
@EnableAsync
public class StandaloneActivemqProducerApplication {

    public static final Logger log = LoggerFactory.getLogger(StandaloneActivemqProducerApplication.class);

    @Value("${spring.activemq.broker-url}")
    private String brokerURL;

    @Value("${spring.activemq.broker-url.remote}")
    private String remoteBrokerURL;

    @Value("${durable.message}")
    private boolean durableMessage;

    public static final String QUEUE_FOR_RECEIVE = "q1";
    public static final String QUEUE_FOR_SEND = "q2";

    public static void main(String[] args) {
        SpringApplication.run(StandaloneActivemqProducerApplication.class, args);
    }

    @Bean
    public Queue mailboxConfirmationQueue() {
        return new ActiveMQQueue(QUEUE_FOR_RECEIVE);
    }

    @Bean
    public CachingConnectionFactory myConnectionFactory() {
        return new CachingConnectionFactory(new ActiveMQConnectionFactory(brokerURL));
    }

    @Bean
    public CachingConnectionFactory remoteConnectionFactory() {
        return new CachingConnectionFactory(new ActiveMQConnectionFactory(remoteBrokerURL));
    }

    @Bean
    public JmsListenerContainerFactory<DefaultMessageListenerContainer> myFactoryListener(CachingConnectionFactory myConnectionFactory,
                                                                                          DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setErrorHandler(t -> log.error("An error has occurred in the transaction: {}", t));
        configurer.configure(factory, myConnectionFactory);
        return factory;
    }

    @Bean
    @Primary
    public MappingJackson2MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

    @Bean
    public JmsTemplate jmsRemoteTemplate(CachingConnectionFactory remoteConnectionFactory, MessageConverter jacksonJmsMessageConverter) {
        JmsTemplate jmsTemplate = new JmsTemplate(remoteConnectionFactory);
        jmsTemplate.setMessageConverter(jacksonJmsMessageConverter);
        jmsTemplate.setExplicitQosEnabled(!durableMessage);
        jmsTemplate.setDeliveryMode(durableMessage ? DeliveryMode.PERSISTENT : DeliveryMode.NON_PERSISTENT);
        return jmsTemplate;
    }
}