package com.endava.example;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;

@Service
@AllArgsConstructor
public class ResponseListener {

    private final Logger log = LoggerFactory.getLogger(ResponseListener.class);

    private final BenchmarkRunner benchmarkRunner;

    @JmsListener(destination = StandaloneActivemqProducerApplication.QUEUE_FOR_RECEIVE, containerFactory = "myFactoryListener")
    public void receiveMessage(
            @Payload String messageContent,
            @Header("startTimeSendToReceiver") String startTimeSendToReceiver,
            @Header("final") boolean isFinal,
            @Header("lastRepeat") boolean isLastRepeat,
            @Header("benchmarkIterationId") int benchmarkId
    ) {
        Instant endTimeReceiveInSender = Instant.now();
        Duration duration = Duration.between(Instant.parse(startTimeSendToReceiver), endTimeReceiveInSender);
        log.debug("Round trip duration {}", duration);
        if(isFinal) {
            log.info("Sending messages finished.");
            benchmarkRunner.finishIteration(
                    endTimeReceiveInSender,
                    benchmarkId,
                    isLastRepeat
            );
        }
    }
}