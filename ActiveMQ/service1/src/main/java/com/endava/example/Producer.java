package com.endava.example;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.*;
import java.time.Instant;

@Service
@RequiredArgsConstructor
public class Producer {

    @Value("${spring.activemq.broker-url}")
    private String brokerURL;
    @Value("${durable.message}")
    private boolean isDurable;

    @Value("${activemq.producer.message-content}")
    private String textMessage;

    private final JmsTemplate jmsRemoteTemplate;

    public void send(int benchmarkId, boolean isLastRepeat) {
        jmsRemoteTemplate.convertAndSend(StandaloneActivemqProducerApplication.QUEUE_FOR_SEND, textMessage, message -> getMessage(message, benchmarkId, isLastRepeat,false));
    }

    public void sendFinalMessage(int benchmarkId, boolean isLastRepeat) {
        jmsRemoteTemplate.convertAndSend(StandaloneActivemqProducerApplication.QUEUE_FOR_SEND, textMessage, message -> getMessage(message, benchmarkId, isLastRepeat,true));
    }

    private Message getMessage(Message message, int benchmarkId, boolean isLastRepeat, boolean isFinal) throws JMSException {
        message.setJMSDeliveryMode(isDurable ? DeliveryMode.PERSISTENT : DeliveryMode.NON_PERSISTENT);
        message.setStringProperty("queueDestination", StandaloneActivemqProducerApplication.QUEUE_FOR_RECEIVE);
        message.setStringProperty("brokerURL", brokerURL);
        message.setBooleanProperty("final", isFinal);
        message.setStringProperty("startTimeSendToReceiver", Instant.now().toString());
        message.setBooleanProperty("lastRepeat", isLastRepeat);
        message.setIntProperty("benchmarkIterationId", benchmarkId);
        return message;
    }
}