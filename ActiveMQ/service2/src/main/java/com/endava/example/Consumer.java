package com.endava.example;

import lombok.RequiredArgsConstructor;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.time.Instant;

@Component
@RequiredArgsConstructor
class Consumer {

    private final Logger log = LoggerFactory.getLogger(Consumer.class);

    private final JmsTemplate jmsRemoteTemplate;
    private final MessageConverter jacksonJmsMessageConverter;

    @Value("${respond.on.new.connection}")
    private boolean respondOnNewConnection;

    @Value("${durable.message}")
    private boolean durableMessage;

    @JmsListener(destination = StandaloneActivemqConsumerApplication.QUEUE_FOR_RECEIVE, containerFactory = "myListenerFactory")
    public void receiveMessage(
            @Payload String messageContent,
            @Header("queueDestination") String queueDestination,
            @Header("brokerURL") String brokerURL,
            @Header("final") boolean isFinal,
            @Header("startTimeSendToReceiver") String startTimeSendToReceiverString,
            @Header("lastRepeat") boolean isLastRepeat,
            @Header("benchmarkIterationId") int benchmarkId
    ) {
        Instant startTimeSendToReceiver = Instant.parse(startTimeSendToReceiverString);
        if (isFinal) {
            log.info("Final message reached!");
        }
        if (respondOnNewConnection) {
            sendResponseWithNewConnection(messageContent, queueDestination, brokerURL, isFinal, startTimeSendToReceiver, benchmarkId, isLastRepeat);
        } else {
            sendResponse(messageContent, queueDestination, isFinal, startTimeSendToReceiver, benchmarkId, isLastRepeat);
        }
    }


    private void sendResponse(String textMessage, String destination, boolean isFinal, Instant startTimeSendToReceiver, int benchmarkId, boolean isLastRepeat) {
        jmsRemoteTemplate.setDeliveryPersistent(false);
        jmsRemoteTemplate.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        jmsRemoteTemplate.convertAndSend(destination, textMessage, message -> {
            message.setStringProperty("startTimeSendToReceiver", startTimeSendToReceiver.toString());
            message.setBooleanProperty("final", isFinal);
            message.setBooleanProperty("lastRepeat", isLastRepeat);
            message.setIntProperty("benchmarkIterationId", benchmarkId);
            return message;
        });
    }

    private void sendResponseWithNewConnection(String textMessage, String destination, String brokerURL, boolean finalMessage,
                                               Instant startTimeSendToReceiver, int benchmarkId, boolean isLastRepeat) {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerURL);

        try (Connection connection = connectionFactory.createConnection()) {
            connection.start();
            try (Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                 MessageProducer producer = session.createProducer(session.createQueue(destination))) {
                producer.setDeliveryMode(durableMessage ? DeliveryMode.PERSISTENT : DeliveryMode.NON_PERSISTENT);

                Message newMessage = jacksonJmsMessageConverter.toMessage(textMessage, session);
                newMessage.setStringProperty("startTimeSendToReceiver", startTimeSendToReceiver.toString());
                newMessage.setBooleanProperty("final", finalMessage);
                newMessage.setBooleanProperty("lastRepeat", isLastRepeat);
                newMessage.setIntProperty("benchmarkIterationId", benchmarkId);

                producer.send(newMessage);
            }
        } catch (JMSException e) {
            log.error(e.getMessage());
        }
    }
}