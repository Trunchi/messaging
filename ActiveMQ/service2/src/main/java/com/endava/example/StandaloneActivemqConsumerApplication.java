package com.endava.example;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import javax.jms.DeliveryMode;
import javax.jms.Queue;

@SpringBootApplication
@EnableJms
public class StandaloneActivemqConsumerApplication {

    private static final Logger log = LoggerFactory.getLogger(StandaloneActivemqConsumerApplication.class);
    public static final String QUEUE_FOR_RECEIVE = "q2";

    @Value("${spring.activemq.broker-url}")
    private String brokerUrl;

    @Value("${spring.activemq.broker-url.remote}")
    private String brokerRemoteUrl;

    @Value("${durable.message}")
    private boolean durableMessage;

    public static void main(String[] args) {
        SpringApplication.run(StandaloneActivemqConsumerApplication.class, args);
    }

    @Bean
    public Queue queue() {
        return new ActiveMQQueue(QUEUE_FOR_RECEIVE);
    }

    @Bean
    public CachingConnectionFactory connectionFactory() {
        return new CachingConnectionFactory(new ActiveMQConnectionFactory(brokerUrl));
    }

    @Bean
    public CachingConnectionFactory connectionRemoteFactory() {
        return new CachingConnectionFactory(new ActiveMQConnectionFactory(brokerRemoteUrl));
    }

    @Bean // Serialize message content to json using TextMessage
    @Primary
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

    @Bean
    public JmsListenerContainerFactory<DefaultMessageListenerContainer> myListenerFactory(
            CachingConnectionFactory connectionFactory,
            DefaultJmsListenerContainerFactoryConfigurer configurer
    ) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setErrorHandler(t -> log.error("An error has occurred in the transaction: {}", t));
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        // You could still override some of Boot's default if necessary.
        return factory;
    }

    @Bean
    public JmsTemplate jmsRemoteTemplate(CachingConnectionFactory connectionRemoteFactory, MessageConverter jacksonJmsMessageConverter) {
        JmsTemplate jmsTemplate = new JmsTemplate(connectionRemoteFactory);
        jmsTemplate.setMessageConverter(jacksonJmsMessageConverter);
        jmsTemplate.setExplicitQosEnabled(!durableMessage);
        jmsTemplate.setDeliveryMode( durableMessage ? DeliveryMode.PERSISTENT : DeliveryMode.NON_PERSISTENT);
        return jmsTemplate;
    }
}
