package com.messaging.rabbit.mq.producers;

import org.springframework.stereotype.Service;

@Service
public class FirstProducer implements FirstProducerIF {
/*       RabbitMq Producers examples

    private static final Logger log = LoggerFactory.getLogger(FirstProducer.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RabbitAdmin admin;

    @Override
//    @Scheduled(fixedDelay = 1000)
    public void sendDirectMessageWithFirstKey(){
        double randomCode = Math.random();
        Message message = new Message("Message from direct exchange with firstKey!", randomCode);
        log.info("Sent:" + message.getText() + message.getCode());
        Object object = rabbitTemplate.convertSendAndReceive(RabbitExchangesConfig.DIRECT_EXCHANGE, RabbitExchangesConfig.FIRST_KEY, message);
        Properties properties = admin.getQueueProperties(RabbitExchangesConfig.FIRST_QUEUE);
        log.info(properties.get("QUEUE_MESSAGE_COUNT").toString());
        if (object == null) {
            log.info("Message in the queue! It's not consumed yet!");
        } else {
            log.info("Message receive back! : " + message.getCode());
        }
    }

    @Override
//    @Scheduled(fixedDelay = 10000)
    public void sendDirectMessageWithSecondKey(){
        double randomCode = Math.random();
        Message message = new Message("Message from direct exchange with secondKey!", randomCode);
        rabbitTemplate.convertAndSend(RabbitExchangesConfig.DIRECT_EXCHANGE, RabbitExchangesConfig.SECOND_KEY, message);
        log.info("Sent:" + message.getText() + message.getCode());
    }

    @Override
//    @Scheduled(fixedDelay = 10000)
    public void sendFanoutMessage(){
        double randomCode = Math.random();
        Message message = new Message("Message from fanout exchange!", randomCode);
        rabbitTemplate.convertAndSend(RabbitExchangesConfig.FANOUT_EXCHANGE,"", message);
        log.info("Sent:" + message.getText() + message.getCode());
    }

    @Override
//    @Scheduled(fixedDelay = 10000)
    public void sendTopicMessage(String routingKey) {
        double randomCode = Math.random();
        //        if (routingKey != null) {
//        Message message = new Message("Message from Topic exchange "+ routingKey + "!", randomCode);
//        rabbitTemplate.convertAndSend(RabbitExchangesConfig.TOPIC_EXCHANGE,routingKey, message);
//        return;
//    }
        List<String> routeKeys = Arrays.asList("queue.marketing", "queue.finance", "queue.admin");
        for (String r : routeKeys) {
            Message message = new Message("Message from Topic exchange "+ r + "!", randomCode);
            rabbitTemplate.convertAndSend(RabbitExchangesConfig.TOPIC_EXCHANGE,r, message);
            log.info("Sent:" + message.getText() + message.getCode());
        }
    }

    @Override
//    @Scheduled(fixedDelay = 10000)
    public void sendHeaderMessages(String department) {

//        if (department == null) {
            List<String> departments = Arrays.asList("marketing", "finance", "admin");
            for (String d : departments) {
                sendHeaderMessageDepartment(d);
            }
//        } else {
//            sendHeaderMessageDepartment(department);
//        }
    }

    private void sendHeaderMessageDepartment(String department) {
        double randomCode = Math.random();
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("department", department);
        MessageConverter messageConverter = new SimpleMessageConverter();
        Message message = new Message("Message from Header exchange + " + department + "!", randomCode);
        org.springframework.amqp.core.Message messageSpring = messageConverter.toMessage(message, messageProperties);
        rabbitTemplate.send(RabbitExchangesConfig.HEADER_EXCHANGE, "", messageSpring);
        log.info("Sent:" + message.getText() + message.getCode());
    }
    */
}

