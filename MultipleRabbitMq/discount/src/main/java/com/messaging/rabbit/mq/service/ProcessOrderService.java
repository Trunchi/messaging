package com.messaging.rabbit.mq.service;

import com.messaging.rabbit.mq.config.RabbitMQConfig;
import com.messaging.rabbit.mq.models.Order;
import com.messaging.rabbit.mq.models.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;


@Service
class ProcessOrderService implements ProcessOrderServiceIF {

    private final Logger log = LoggerFactory.getLogger(ProcessOrderService.class);

    @Autowired
    private RabbitTemplate rabbitTemplateForShop;

    @Autowired
    private Jackson2JsonMessageConverter producerMessageConverter;

    @Autowired
    private RabbitAdmin admin;

    @Autowired
    TimeKeeper timeKeeper;

    @Component
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    class TimeKeeper {

        private Long start;

        public Long getStart() {
            return start;
        }

        public void setStart(Long start) {
            this.start = start;
        }
    }

    @Override
    public void receiveOrderAndProcessIt(final Message message) {
        long receiveTime = System.currentTimeMillis();
        if (timeKeeper.getStart() == null) {
            timeKeeper.setStart(receiveTime);
        }

        Order order = (Order) producerMessageConverter.fromMessage(message);
        order.setEndTimeReceiveInDiscount(receiveTime);

        long time = TimeUnit.MILLISECONDS.toSeconds(receiveTime - order.getStartTimeSendToDiscount());
        log.info("Order ID : " + order.getId() + " received to be process in: " + time + " seconds");

        String exchange = message.getMessageProperties().getHeaders().get("respondToExchange").toString();
        String routingKey = message.getMessageProperties().getHeaders().get("respondToRoutingKey").toString();

        giveDiscountAndSend(order, exchange, routingKey);

        Integer messagesCount = Integer.parseInt(admin.getQueueProperties(RabbitMQConfig.OPEN_ORDERS_DURABLE_QUEUE).get("QUEUE_MESSAGE_COUNT").toString());
        if (messagesCount < 2) {
            long end = System.currentTimeMillis();
            log.info("Almost done in " + TimeUnit.MILLISECONDS.toSeconds(end - timeKeeper.getStart()) + " seconds!");
            timeKeeper.setStart(end);
        }
    }

    private void giveDiscountAndSend(Order order, String exchange, String routingKey) {
        Double sumProducts = order != null && order.getProducts() != null ? order.getProducts().stream().mapToDouble(p -> p.getPrice()).sum() : null;
        if (sumProducts == null) {
            throw new IllegalArgumentException("Something wrong with order id " + (order != null ? order.getId() : null));
        }

        int discount = 0;
        if (sumProducts > 100000) {
            discount = (int) (sumProducts * (order.getProducts().size() > 20 ? 20 : order.getProducts().size()) / 100);
        }
        order.setStatus(discount == 0 ? Status.WITHOUT_DISCOUNT : Status.WITH_DISCOUNT);
        order.setDiscount(discount);
        long sendToShopTime = System.currentTimeMillis();
        order.setStartTimeSendToShop(sendToShopTime);
        rabbitTemplateForShop.convertAndSend(exchange, routingKey, order);
    }
}
