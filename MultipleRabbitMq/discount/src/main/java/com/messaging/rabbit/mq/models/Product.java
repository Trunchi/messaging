package com.messaging.rabbit.mq.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {

    private final double id;
    private final double price;
    private final String description;

    public Product(@JsonProperty("id") double id,  @JsonProperty("price") double price, @JsonProperty("description") String description) {
        this.id = id;
        this.price = price;
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public double getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Product{" +
                "price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}