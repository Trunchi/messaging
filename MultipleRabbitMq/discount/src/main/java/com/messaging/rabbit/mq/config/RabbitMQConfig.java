package com.messaging.rabbit.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitMQConfig {

    public static final String OPEN_ORDERS_DURABLE_QUEUE = "openOrdersDurableQueue";
    public static final String PROCESSED_ORDERS_DURABLE_QUEUE = "processedOrdersDurableQueue";
    public static final String DIRECT_EXCHANGE = "directExchange";
    public static final String OPEN_ORDERS_KEY = "openOrders";

    @Bean Queue openOrdersDurableQueue() {
        return new Queue(OPEN_ORDERS_DURABLE_QUEUE, true);
    }

    @Bean Queue processedOrdersDurableQueue() {
        return new Queue(PROCESSED_ORDERS_DURABLE_QUEUE, true);
    }

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(DIRECT_EXCHANGE);
    }

    @Bean
    public Binding processedOrdersBinding () {
        return BindingBuilder.bind(openOrdersDurableQueue()).to(directExchange()).with(OPEN_ORDERS_KEY);
    }

    @Bean
    public Jackson2JsonMessageConverter producerMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplateForDiscount(final ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplateForDiscount = new RabbitTemplate(connectionFactory);
        rabbitTemplateForDiscount.setMessageConverter(producerMessageConverter());
        return rabbitTemplateForDiscount;
    }

    @Bean
    public RabbitTemplate rabbitTemplateForShop() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("localhost",5673);
        RabbitTemplate rabbitTemplateForShop = new RabbitTemplate(connectionFactory);
        rabbitTemplateForShop.setMessageConverter(producerMessageConverter());
        return rabbitTemplateForShop;
    }

    @Bean
    public RabbitAdmin admin (RabbitTemplate rabbitTemplateForDiscount) {
        return new RabbitAdmin(rabbitTemplateForDiscount);
    }

/*

    Examples of rabbitMQ exchanges, queues and bindings

    public static final String FANOUT_EXCHANGE = "fanoutExchange";
    public static final String TOPIC_EXCHANGE = "topicExchange";
    public static final String HEADER_EXCHANGE = "headerExchange";
    public static final String FIRST_QUEUE = "firstQueue";
    public static final String SECOND_QUEUE = "secondQueue";
    public static final String THIRD_QUEUE = "thirdQueue";
    public static final String FIRST_KEY = "FirstKey";
    public static final String SECOND_KEY = "SecondKey";
    public static final String THIRD_KEY = "ThirdKey";
    public static final String MARKETING_QUEUE = "marketingQueue";
    public static final String FINANCE_QUEUE = "financeQueue";
    public static final String ADMIN_QUEUE = "adminQueue";
    public static final String ALL_QUEUE = "allQueue";

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange(TOPIC_EXCHANGE);
    }

    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange(FANOUT_EXCHANGE);
    }

    @Bean
    public HeadersExchange headerExchange() {
        return new HeadersExchange(HEADER_EXCHANGE);
    }

    @Bean
    public Queue firstQueue() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-max-length", 10);
        args.put("x-overflow", "reject-publish");
        return new Queue(FIRST_QUEUE,true,false,false, args);
    }

    @Bean
    public Queue secondQueue() {
        return new Queue(SECOND_QUEUE);
    }

    @Bean
    Queue marketingQueue() {
        return new Queue(MARKETING_QUEUE);
    }

    @Bean
    Queue financeQueue() {
        return new Queue(FINANCE_QUEUE);
    }

    @Bean
    Queue adminQueue() {
        return new Queue(ADMIN_QUEUE);
    }

    @Bean
    Queue allQueue() {
        return new Queue(ALL_QUEUE, false);
    }

    @Bean
    RabbitAdmin getRabbitAdmin() {
        return new RabbitAdmin(rabbitTemplate);
    }

    @Bean
    Binding marketingBinding(Queue marketingQueue, HeadersExchange headerExchange) {
        return BindingBuilder.bind(marketingQueue).to(headerExchange).where("department").matches("marketing");
    }

    @Bean
    Binding financeBinding(Queue financeQueue, HeadersExchange headerExchange) {
        return BindingBuilder.bind(financeQueue).to(headerExchange).where("department").matches("finance");
    }

    @Bean
    Binding adminBinding(Queue adminQueue, HeadersExchange headerExchange) {
        return BindingBuilder.bind(adminQueue).to(headerExchange).where("department").matches("admin");
    }

    @Bean
    Binding allBinding(Queue allQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(allQueue).to(topicExchange).with("queue.*");
    }

    @Bean
    public Binding firstQueueToDirectExchangeWithFirstKeyBinding() {
        return BindingBuilder.bind(firstQueue()).to(directExchange()).with(FIRST_KEY);
    }

    @Bean
    public Binding secondQueueToDirectExchangeWithSecondKeyBinding() {
        return BindingBuilder.bind(secondQueue()).to(directExchange()).with(SECOND_KEY);
    }

    @Bean
    public Binding firstQueueToFanoutExchangeBinding() {
        return BindingBuilder.bind(firstQueue()).to(fanoutExchange());
    }

    @Bean
    public Binding secondQueueToFanoutExchangeBinding() {
        return BindingBuilder.bind(secondQueue()).to(fanoutExchange());
    }

 */
}
