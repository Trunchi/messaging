package com.messaging.rabbit.mq.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Random;

public class Product {

    @JsonProperty("id")
    private double id;
    @JsonProperty("price")
    private double price;
    private String description;

    public Product( @JsonProperty("description") String description) {
        this.id = new Random().nextInt(1000000);
        this.price = new Random().nextInt(1000000);
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public double getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Product{" +
                "price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
