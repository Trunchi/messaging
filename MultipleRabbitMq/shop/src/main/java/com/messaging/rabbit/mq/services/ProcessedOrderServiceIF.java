package com.messaging.rabbit.mq.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.messaging.rabbit.mq.config.RabbitMQConfig;
import com.messaging.rabbit.mq.models.Order;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.scheduling.annotation.Scheduled;

public interface ProcessedOrderServiceIF {
    @RabbitListener(queues = RabbitMQConfig.PROCESSED_ORDERS_DURABLE_QUEUE)
    void receiveOrderProcessed(Order order);

    String sendOrder(int iterations) throws JsonProcessingException;
}
