package com.messaging.rabbit.mq.services;

import com.messaging.rabbit.mq.models.Order;
import com.messaging.rabbit.mq.models.Product;
import com.messaging.rabbit.mq.models.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.MessagePropertiesBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
class ProcessedOrderService implements ProcessedOrderServiceIF {
    private final Logger log = LoggerFactory.getLogger(ProcessedOrderService.class);

    @Autowired
    private RabbitTemplate rabbitTemplateSendToClient;

    @Value("${routing.key.for.sending.products}")
    private String routingKey;

    @Value("${routing.key.to.use.for.receiving.processed.order}")
    private String routingKeyWhereToReceive;

    @Value("${exchange.to.use.for.receiving.processed.order}")
    private String exchangeWhereToReceive;

    @Value("${exchange.type.for.sending.products}")
    private String exchangeTypeForSendingProducts;

    @Autowired
    private Jackson2JsonMessageConverter producerMessageConverter;

    @Override
    public void receiveOrderProcessed(final Order order) {
        long receiveTime = System.currentTimeMillis();
        long receiveTimeAfterOrderProcessed = TimeUnit.MILLISECONDS.toSeconds(receiveTime - order.getStartTimeSendToShop());
        long receiveTimeFromProcessBeginning = TimeUnit.MILLISECONDS.toSeconds(receiveTime - order.getStartTimeSendToDiscount());
        log.info("Order ID : " + order.getId() + " all process time in: " + receiveTimeFromProcessBeginning + " Status: " + order.getStatus() + " Discount: " + order.getDiscount());
        log.info("Order ID : " + order.getId() + " processed in: " + receiveTimeAfterOrderProcessed + " Status: " + order.getStatus() + " Discount: " + order.getDiscount());
    }

    @Override
    public String sendOrder(int iterations) {
        long start = System.currentTimeMillis();
        List<Product> products = Arrays.asList(new Product("Socks"), new Product("Socks"),new Product("Socks"), new Product("Socks"),
                new Product("Socks"), new Product("Socks"),new Product("Socks"), new Product("Socks"),new Product("Socks"), new Product("Socks"),
                new Product("Socks"), new Product("Socks"),new Product("Socks"), new Product("Socks"),new Product("Socks"), new Product("Socks"),
                new Product("Socks"), new Product("Socks"),new Product("Socks"));
        for (int i = 0 ;i < iterations ; i++) {
            Order order = new Order(products, 0, Status.OPEN);
            MessageProperties props = MessagePropertiesBuilder.newInstance().setContentType(MessageProperties.CONTENT_TYPE_JSON).build();
            props.setHeader("respondToExchange", exchangeWhereToReceive);
            props.setHeader("respondToRoutingKey", routingKeyWhereToReceive);
            long startTime = System.currentTimeMillis();
            order.setStartTimeSendToDiscount(startTime);
            Message message = producerMessageConverter.toMessage(order, props);
            rabbitTemplateSendToClient.convertAndSend(exchangeTypeForSendingProducts, routingKey, message);
        }
        long end = System.currentTimeMillis();
        log.info("For " + iterations + " messages took : " + TimeUnit.MILLISECONDS.toSeconds(end - start));
        return "For " + iterations + " messages took : " + TimeUnit.MILLISECONDS.toSeconds(end - start);
    }
}
