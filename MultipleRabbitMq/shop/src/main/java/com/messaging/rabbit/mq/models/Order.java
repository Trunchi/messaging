package com.messaging.rabbit.mq.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Random;

public class Order {

    private final List<Product> products;

    private int discount;

    private Status status;

    @JsonProperty
    private long startTimeSendToDiscount;

    @JsonProperty
    private long endTimeReceiveInDiscount;

    @JsonProperty
    private long startTimeSendToShop;

    @JsonProperty
    private long endTimeReceiveInShop;

    @JsonProperty
    private final double id;

    public Order(@JsonProperty("products") List<Product> products, @JsonProperty("discount") int discount, @JsonProperty("status") Status status) {
        this.id = new Random().nextInt(1000000000);
        this.products = products;
        this.discount = discount;
        this.status = status;
    }

    public double getId() {
        return this.id;
    }

    public long getStartTimeSendToDiscount() {
        return startTimeSendToDiscount;
    }

    public long getEndTimeReceiveInDiscount() {
        return endTimeReceiveInDiscount;
    }

    public long getStartTimeSendToShop() {
        return startTimeSendToShop;
    }

    public long getEndTimeReceiveInShop() {
        return endTimeReceiveInShop;
    }

    public void setStartTimeSendToDiscount(long startTimeSendToDiscount) {
        this.startTimeSendToDiscount = startTimeSendToDiscount;
    }

    public void setEndTimeReceiveInDiscount(long endTimeReceiveInDiscount) {
        this.endTimeReceiveInDiscount = endTimeReceiveInDiscount;
    }

    public void setStartTimeSendToShop(long startTimeSendToShop) {
        this.startTimeSendToShop = startTimeSendToShop;
    }

    public void setEndTimeReceiveInShop(long endTimeReceiveInShop) {
        this.endTimeReceiveInShop = endTimeReceiveInShop;
    }

    public List<Product> getProducts() {
        return products;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}