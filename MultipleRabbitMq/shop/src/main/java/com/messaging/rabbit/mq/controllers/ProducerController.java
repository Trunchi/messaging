package com.messaging.rabbit.mq.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.messaging.rabbit.mq.services.ProcessedOrderServiceIF;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProducerController {
    private final ProcessedOrderServiceIF producerService;

    @GetMapping("benchmark/{iterations}")
    public String benchmark(@PathVariable int iterations) throws JsonProcessingException {
        return producerService.sendOrder(iterations);
    }
}
