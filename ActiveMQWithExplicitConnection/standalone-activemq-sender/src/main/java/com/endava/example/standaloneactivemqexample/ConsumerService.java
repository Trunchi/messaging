package com.endava.example.standaloneactivemqexample;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

@Service
@AllArgsConstructor
public class ConsumerService {

    private final Logger log = LoggerFactory.getLogger(ConsumerService.class);

    @JmsListener(destination = "mailboxConfirmation", containerFactory = "myFactoryListener")
    public void receiveMessage(
            @Payload String messageContent,
            @Header("startTimeSendToReceiver") String startTimeSendToReceiver,
            @Header("iteration") String id
    ) {
        Instant time = Instant.now();
        Duration duration = Duration.between(Instant.parse(startTimeSendToReceiver), time);
        log.debug("Message with id: " + id + " received in : " + duration + " seconds.");
    }

}
