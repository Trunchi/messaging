package com.endava.example.standaloneactivemqexample;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.jms.Queue;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class ProducerService {

    @Value("${queue.where.to.receive}")
    private String queueForReceive;

    @Value("${spring.activemq.broker-url}")
    private String brokerURL;

    private final JmsTemplate jmsRemoteTemplate;

    private final Queue mailboxQueue;

    private final Logger log = LoggerFactory.getLogger(ProducerService.class);

    @Async
    public String send(int iterations) {
        long start = System.currentTimeMillis();
        log.info("Start to send " + iterations + " messages.");
        String textMessage = "81v4NmGjNy0Dmz5jy1ZELk0IVGSiGGqGmTdbFC6Y237wxz8io2vBIqWh58rzEwI6ODiFNMeAsIna0kdBgyJssGWdKCKDA3wvlcwzoQ1EAmngMHZaJlOrymLYHXHgmlNLItY243V90MMEQv8AdKRz1ZhbYYQOb1ULhTTB6x12ILZjNTJaUadJEMYSN5mR0LE4AU0gRCLSvLMpadLGgZvbPVsflSrmSSvAX4QVu5zmabTYgvpb2gKD5wtWtvA4ilZC47kYBF5hnKYgWjwCSBnEtSOPrgC4cLf4ULVhJec5MuxraMxyN1bcFVOPMfx8lDJqWZxyPWD6WrpHASlX7cCWOhBrunkKytVkLtK1J9Xmts8150dfeRUaZdJ2gk9hrPqrHCo8jcZNv6W6EnkrGlV7tOsTkDubvFMlxiSQNpi6yOuPcOmdGo6BectUfWBCNzan9oKHrJLGq0bozOwZRvyHNCkcZbiOyUuzzpwFrn6uEkVNuiHQcqiNrU4gzGXSFCUDJ5RMsWGPAMUi9srttXovcptbgQIQMC5Bype7DdX2cQyKQccXuZnJwptOLWygicmYlwjbw7MvHagVzgkBP94mFlqDgKyK1RHXkX484v5CodNJDWzq0g687L4AfWT5ATKc8AlSKnfzCl13JZwDyY0rfDiuoDmirHx7iArQGsqhYAS5VvKhgJlXsoSzgRyKCTh8Dk8dSd2lV84jZvx1WVbmEwab3kkf0MTpOqKx5l8cE47RzqANTzRr8IEySRGIC5WLI7AM9lYEh6NVLDx4OZAH2vYUAVRfCXQMna8OiG6grzE16xS91RNpvffFyd7DM7VXS12IUrJLHOZ57GgaZy7EIV1fXtCmr6jOjj2FZTSFNxGYWkBFTAXw2sWenNZ4zz5E2MQ333iYkJzHTv1gO5soYe6sdJDepKbspMi30hf4AYnTan0QlJ9Q3hk99YshCOMrjqPYCpUUwrSJpdfLNiCPCy4Cv4mFJIdBA7bbEZ7O";

        Instant startSend = Instant.now();
        for (int i = 0; i <= iterations - 1; i++) {
//            Instant startSend = Instant.now();
//            jmsRemoteTemplate.send("mailbox", session -> getMessage(id, textMessage, session, startSend, false));
            sendMessage(textMessage, startSend, i, false);
        }
        // Last message
        sendMessage(textMessage, startSend, iterations, true);

        long end = System.currentTimeMillis();
        log.info("Sent " + iterations + " messages in " + TimeUnit.MILLISECONDS.toSeconds(end - start) + "seconds");
        return "Sent " + iterations + " messages in " + TimeUnit.MILLISECONDS.toSeconds(end - start) + "seconds";
    }

    private void sendMessage(String textMessage, Instant startSend, int id, boolean isFinal) {
        jmsRemoteTemplate.convertAndSend(mailboxQueue, textMessage, message -> {
            message.setStringProperty("queueDestination", queueForReceive);
            message.setStringProperty("brokerURL", brokerURL);
            message.setBooleanProperty("final", isFinal);
            message.setStringProperty("startTimeSendToReceiver", startSend.toString());
            message.setStringProperty("iteration", String.valueOf(id));
            return message;
        });
    }

/*    private Message getMessage(int id, String textMessage, Instant startSend, boolean finalMessage) {
        Message message = jacksonJmsMessageConverter.toMessage(textMessage, session);
        message.setStringProperty("queueDestination", queueForReceive);
        message.setStringProperty("brokerURL",brokerURL);
        message.setBooleanProperty("final", finalMessage);
        message.setStringProperty("startTimeSendToReceiver", startSend.toString());
        message.setStringProperty("id", String.valueOf(id));
        return message;
    }*/
}