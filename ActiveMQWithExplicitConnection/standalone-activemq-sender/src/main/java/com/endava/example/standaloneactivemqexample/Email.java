package com.endava.example.standaloneactivemqexample;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.activemq.command.ActiveMQTextMessage;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Email implements Serializable {
    private String to;
    private String body;
    private EmailStatus emailStatus;
    private long startTimeSendToReceiver;
    private long endTimeReceiveInReceiver;
    private long startTimeSendToSender;
    private long endTimeReceiveInSender;

    public Email(String to, String body, EmailStatus emailStatus) {
        super();
        this.to = to;
        this.body = body;
        this.emailStatus = emailStatus;
    }
}