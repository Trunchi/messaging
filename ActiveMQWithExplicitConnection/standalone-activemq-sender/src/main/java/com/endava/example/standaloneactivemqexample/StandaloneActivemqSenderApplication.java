package com.endava.example.standaloneactivemqexample;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.jms.Queue;

@SpringBootApplication
@EnableJms
@EnableAsync
public class StandaloneActivemqSenderApplication {

    @Value("${spring.activemq.broker-url}")
    private String brokerURL;

    @Value("${spring.activemq.broker-url.remote}")
    private String remoteBrokerUrl;

    @Value("${queue.where.to.receive}")
    private String queueForReceive;

    public static void main(String[] args) {
        SpringApplication.run(StandaloneActivemqSenderApplication.class, args);
    }

    @Bean
    public Queue mailboxQueue() {
        return new ActiveMQQueue("mailbox");
    }

    @Bean
    public Queue mailboxConfirmationQueue() {
        return new ActiveMQQueue(queueForReceive);
    }

    @Bean
    public CachingConnectionFactory myConnectionFactory() {
        return new CachingConnectionFactory(new ActiveMQConnectionFactory(brokerURL));
    }

    @Bean
    public CachingConnectionFactory remoteConnectionFactory() {
        new CachingConnectionFactory();
        return new CachingConnectionFactory(new ActiveMQConnectionFactory(remoteBrokerUrl));
    }

    @Bean
    public JmsListenerContainerFactory<?> myFactoryListener(CachingConnectionFactory myConnectionFactory,
                                                            DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setErrorHandler(t -> System.err.println("An error has occurred in the transaction: " + t));
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, myConnectionFactory);
        // You could still override some of Boot's default if necessary.
        return factory;
    }

    /*

        @Bean // Serialize message content to json using TextMessage
        @Primary
        public MappingJackson2MessageConverter jacksonJmsMessageConverter() {
            MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
            converter.setTargetType(MessageType.TEXT);
            converter.setTypeIdPropertyName("_type");
            return converter;
        }
    */
    @Bean // Serialize message content to json using TextMessage
    @Primary
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

    @Bean
    public JmsTemplate jmsRemoteTemplate(CachingConnectionFactory remoteConnectionFactory, MessageConverter jacksonJmsMessageConverter) {
        JmsTemplate jmsTemplate = new JmsTemplate(remoteConnectionFactory);
        jmsTemplate.setMessageConverter(jacksonJmsMessageConverter);
        return jmsTemplate;
    }
}
