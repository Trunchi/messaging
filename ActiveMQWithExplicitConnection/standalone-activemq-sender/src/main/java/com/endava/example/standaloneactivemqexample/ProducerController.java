package com.endava.example.standaloneactivemqexample;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProducerController {
    private final ProducerService producerService;

    @GetMapping("benchmark/{iterations}")
    public String benchmark(@PathVariable int iterations) {
        return producerService.send(iterations);
    }
}
