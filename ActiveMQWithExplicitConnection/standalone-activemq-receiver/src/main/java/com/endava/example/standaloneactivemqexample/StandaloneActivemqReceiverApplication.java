package com.endava.example.standaloneactivemqexample;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQObjectMessage;
import org.apache.activemq.command.ActiveMQQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableJms
public class StandaloneActivemqReceiverApplication {

    @Value("${spring.activemq.broker-url}")
    private String brokerUrl;

    public static void main(String[] args) {
        SpringApplication.run(StandaloneActivemqReceiverApplication.class, args);
    }

    @Bean
    public Queue queue() {
        return new ActiveMQQueue("mailbox");
    }

    @Bean
    public ActiveMQConnectionFactory connectionFactory() {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(brokerUrl);
        return activeMQConnectionFactory;
    }

    @Bean // Serialize message content to json using TextMessage
    @Primary
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

    @Bean
    public JmsListenerContainerFactory<?> myListenerFactory(ActiveMQConnectionFactory connectionFactory,
                                                    DefaultJmsListenerContainerFactoryConfigurer configurer,
                                                    MessageConverter jacksonJmsMessageConverter) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setErrorHandler(t -> System.err.println("An error has occurred in the transaction: " + t));
        factory.setMessageConverter(jacksonJmsMessageConverter);
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        // You could still override some of Boot's default if necessary.
        return factory;
    }

    @Component
    @AllArgsConstructor
    class Receiver {

        MessageConverter jacksonJmsMessageConverter;
        TimeKeeper timeKeeper;
        private final Logger log = LoggerFactory.getLogger(Receiver.class);

        @JmsListener(destination = "mailbox", containerFactory = "myListenerFactory")
        public void receiveMessage(Message message) throws JMSException {
            Instant time = Instant.now();
            if (timeKeeper.getStart() == null) {
                timeKeeper.setStart(time);
            }

            String destination = message.getStringProperty("queueDestination");
            String brokerURL = message.getStringProperty("brokerURL");
            boolean finalMessage = message.getBooleanProperty("final");
            String id = message.getStringProperty("id");
            String textMessage = (String) jacksonJmsMessageConverter.fromMessage(message);
            Instant startTimeSendToReceiver = Instant.parse(message.getStringProperty("startTimeSendToReceiver"));

            log.info("Message with id: " + message.getStringProperty("id") + " received in : " + Duration.between(startTimeSendToReceiver,time) + "seconds.");
            send(textMessage, destination, brokerURL, finalMessage, startTimeSendToReceiver, time, id);
        }

        private void send(String textMessage, String destination, String brokerURL, boolean finalMessage,
                          Instant startTimeSendToReceiver, Instant receiveTime, String id) {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerURL);

            try {
                Connection connection = connectionFactory.createConnection();
                connection.start();
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                //Setup a message producer to send message to the queue the server is consuming from
                MessageProducer producer = session.createProducer(session.createQueue(destination));
                producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
                Instant time = Instant.now();
                if (finalMessage) {
                    log.info("Final message reached. Took " + Duration.between(timeKeeper.getStart(), time) + " seconds");
                    timeKeeper.setStart(null);
                }
                Message newMessage = jacksonJmsMessageConverter.toMessage(textMessage, session);
                newMessage.setStringProperty("startTimeSendToReceiver", startTimeSendToReceiver.toString());
                newMessage.setStringProperty("endTimeReceiveInReceiver", receiveTime.toString());
                newMessage.setStringProperty("id", id);
                producer.send(newMessage);
                producer.close();
                session.close();
                connection.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }

    @Component
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    @Getter
    @Setter
    class TimeKeeper {
        private Instant start;
    }
}
