package com.endava.example.standaloneactivemqexample;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Email {
    private String to;
    private String body;
    private EmailStatus emailStatus;
    private long startTimeSendToReceiver;
    private long endTimeReceiveInReceiver;
    private long startTimeSendToSender;
    private long endTimeReceiveInSender;

    public Email(String to, String body, EmailStatus emailStatus) {
        super();
        this.to = to;
        this.body = body;
        this.emailStatus = emailStatus;
    }
}
