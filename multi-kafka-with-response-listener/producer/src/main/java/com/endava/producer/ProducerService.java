package com.endava.producer;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ProducerService {

    public static final Logger LOGGER = LoggerFactory.getLogger(ProducerService.class);

    private final KafkaTemplate<String,String> kafkaTemplate;

    public void start() {
        Message<String> message = MessageBuilder.withPayload("<Message content>")
                .setHeader(KafkaHeaders.TOPIC, ProducerApplication.TOPIC)
                .setHeader(KafkaHeaders.MESSAGE_KEY, "999")
                .setHeader(KafkaHeaders.PARTITION_ID, 0)
                .setHeader("respondToHost", "localhost")
                .setHeader("respondToPort", "9092")
                .setHeader("respondToTopic", "t2")
                .build();
        LOGGER.debug("Sending message");
        kafkaTemplate.send(message);
    }
}
