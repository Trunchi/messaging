package com.endava.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerApplication {

	public static final String TOPIC = "t1";

	public static void main(String[] args) {
		SpringApplication.run(ProducerApplication.class, args);
	}

}
