package com.endava.producer;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("kafka")
@AllArgsConstructor
public class ProducerController {

    private final ProducerService producerService;

    @GetMapping("produce")
    public void produce() {
        producerService.start();
    }
}
