package com.endava.consumer;

import org.apache.kafka.common.protocol.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class ConsumerService {

    public static final Logger LOGGER = LoggerFactory.getLogger(ConsumerService.class);

    @KafkaListener(topics = "t1", groupId = "g1")
    public void listen(
            @Payload String data,
            @Header("respondToHost") String respondToHost,
            @Header("respondToPort") String respondToPort,
            @Header("respondToTopic") String respondToTopic
    ) {
        LOGGER.info("Received: " + data + ", " + respondToHost + respondToPort + respondToTopic);
    }
}
