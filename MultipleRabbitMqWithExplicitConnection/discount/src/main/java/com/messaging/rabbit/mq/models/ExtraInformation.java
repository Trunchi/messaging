package com.messaging.rabbit.mq.models;

public abstract class ExtraInformation {
    private String routingKey;
    private String exchangeType;
    private String selfApplicationHost;
    private String selfApplicationPort;

    public ExtraInformation(String selfApplicationHost, String selfApplicationPort, String routingKey, String exchangeType) {
        this.routingKey = routingKey;
        this.exchangeType = exchangeType;
        this.selfApplicationHost = selfApplicationHost;
        this.selfApplicationPort = selfApplicationPort;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public String getExchangeType() {
        return exchangeType;
    }

    public String getSelfApplicationHost() {
        return selfApplicationHost;
    }

    public String getSelfApplicationPort() {
        return selfApplicationPort;
    }
}
