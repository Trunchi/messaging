package com.messaging.rabbit.mq.models;

import java.io.Serializable;

public class Message implements Serializable {

    private String text;
    private double code;

    public Message() {
    }

    public Message(String text, double code) {
        this.text = text;
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public double getCode() {
        return code;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setCode(double code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "PracticalTipMessage{" +
                "text='" + text + '\'' +
                ", code=" + code +
                '}';
    }
}
