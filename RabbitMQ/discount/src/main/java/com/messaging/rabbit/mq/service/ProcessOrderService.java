package com.messaging.rabbit.mq.service;

import com.messaging.rabbit.mq.models.Order;
import com.messaging.rabbit.mq.models.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
class ProcessOrderService implements ProcessOrderServiceIF {

    private final Logger log = LoggerFactory.getLogger(ProcessOrderService.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void receiveOrderAndProcessIt(final Order order)  {
        Double sumProducts = order != null && order.getProducts() != null ? order.getProducts().stream().mapToDouble( p -> p.getPrice()).sum() : null ;
        if (sumProducts == null) {
            log.info("Something wrong with order id " + (order != null ? order.getId() : null));
            return;
        }
        int discount = 0;
        if (sumProducts > 100000) {
            discount = (int) (sumProducts * (order.getProducts().size() > 20 ? 20 : order.getProducts().size()) / 100);
        }
        log.info("Products received: " + order.toString());
        order.setStatus(discount == 0 ? Status.WITHOUT_DISCOUNT : Status.WITH_DISCOUNT);
        order.setDiscount(discount);
        rabbitTemplate.convertAndSend(order.getExchangeType(), order.getRoutingKey(), order);
    }
}