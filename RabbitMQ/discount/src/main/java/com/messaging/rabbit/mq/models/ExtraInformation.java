package com.messaging.rabbit.mq.models;

public abstract class ExtraInformation {
    private String routingKey;
    private String exchangeType;

    public ExtraInformation(String routingKey, String exchangeType) {
        this.routingKey = routingKey;
        this.exchangeType = exchangeType;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public String getExchangeType() {
        return exchangeType;
    }
}