package com.messaging.rabbit.mq.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Random;

public class Order extends ExtraInformation {

    private final List<Product> products;

    private int discount;

    private Status status;

    @JsonProperty
    private final double id;

    public Order(@JsonProperty("routingKey") String routingKey, @JsonProperty("exchangeType") String exchangeType, @JsonProperty("products") List<Product> products, @JsonProperty("discount") int discount, @JsonProperty("status") Status status) {
        super(routingKey,exchangeType);
        this.id = new Random().nextInt(1000000);
        this.products = products;
        this.discount = discount;
        this.status = status;
    }

    public double getId() {
        return this.id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Order{" +
                "products=" + products +
                ", discount=" + discount +
                ", status=" + status +
                ", id=" + id +
                '}';
    }
}
