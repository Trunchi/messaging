package com.messaging.rabbit.mq.service;

import com.messaging.rabbit.mq.config.RabbitMQConfig;
import com.messaging.rabbit.mq.models.Order;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

public interface ProcessOrderServiceIF {
    @RabbitListener(queues = RabbitMQConfig.OPEN_ORDERS_DURABLE_QUEUE)
    void receiveOrderAndProcessIt(Order order);
}
