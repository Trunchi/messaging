package com.messaging.rabbit.mq.consumers;

import com.messaging.rabbit.mq.config.RabbitMQConfig;
import com.messaging.rabbit.mq.models.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.utils.SerializationUtils;
import org.springframework.stereotype.Service;


public class FirstConsumer {
/*      RabbitListeners examples

    private static final Logger log = LoggerFactory.getLogger(FirstConsumer.class);

    @RabbitListener(queues = RabbitExchangesConfig.FIRST_QUEUE)
    public boolean consumeFirstQueueMessage(final Message message) {
        log.info("Received :First Queue! : " + message.getText() + " Code:" + message.getCode());
        return true;
    }

    @RabbitListener(queues = RabbitExchangesConfig.SECOND_QUEUE)
    public void consumeSecondQueueMessage(final Message message) {
        log.info("Received : Second Queue! : " + message.getText() + " Code:" + message.getCode());
    }

    @RabbitListener(queues = RabbitExchangesConfig.ADMIN_QUEUE)
    public void consumeAdminQueueMessage(final org.springframework.amqp.core.Message message) {
        Message message1 = (Message) SerializationUtils.deserialize(message.getBody());
        log.info("Received :Admin Queue! : " + message1.getText() + " Code:" + message1.getCode());
    }

    @RabbitListener(queues = RabbitExchangesConfig.FINANCE_QUEUE)
    public void consumeFinanceQueueMessage(final org.springframework.amqp.core.Message message) {
        Message message1 = (Message) SerializationUtils.deserialize(message.getBody());
        log.info("Received : FinanceQ Queue! : " + message1.getText() + " Code:" + message1.getCode());
    }

    @RabbitListener(queues = RabbitExchangesConfig.MARKETING_QUEUE)
    public void consumeMarketingQueueMessage(final org.springframework.amqp.core.Message message) {
        Message message1 = (Message) SerializationUtils.deserialize(message.getBody());
        log.info("Received :Marketing Queue! : " + message1.getText() + " Code:" + message1.getCode());
    }

    @RabbitListener(queues = RabbitExchangesConfig.ALL_QUEUE)
    public void consumeAllQueueMessage(final Message message) {
        log.info("Received :All Queue! : " + message.getText() + " Code:" + message.getCode());
    }
*/
}
