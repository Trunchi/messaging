package com.messaging.rabbit.mq.config;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JacksonConfig {

    @Bean
    public Jackson2JsonMessageConverter producerMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerMessageConverter());
        return rabbitTemplate;
    }

//    @Bean
//    public RabbitTemplate rabbitTemplate1() {
//        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("localhost",5673);
//        RabbitTemplate rabbitTemplate1 = new RabbitTemplate(connectionFactory);
//        rabbitTemplate1.setMessageConverter(producerMessageConverter());
//        return rabbitTemplate1;
//    }
}
