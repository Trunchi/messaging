package com.messaging.rabbit.mq1.messaging.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.messaging.rabbit.mq1.messaging.config.RabbitMQConfig;
import com.messaging.rabbit.mq1.messaging.models.Order;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.scheduling.annotation.Scheduled;

public interface ProcessedOrderServiceIF {
    @RabbitListener(queues = RabbitMQConfig.PROCESSED_ORDERS_DURABLE_QUEUE)
    void receiveOrderProcessed(Order order);

    @Scheduled(fixedDelay = 1000)
    void sendOrder() throws JsonProcessingException;
}
