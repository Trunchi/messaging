package com.messaging.rabbit.mq1.messaging.config;

import com.messaging.rabbit.mq1.messaging.MessagingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
    public static final String OPEN_ORDERS_DURABLE_QUEUE = "openOrdersDurableQueue";
    public static final String PROCESSED_ORDERS_DURABLE_QUEUE = "processedOrdersDurableQueue";
    public static final String DIRECT_EXCHANGE = "directExchange";

    private static final Logger log = LoggerFactory.getLogger(RabbitMQConfig.class);

    @Bean
    Queue ordersOpenedDurableQueue() {
        return new Queue(OPEN_ORDERS_DURABLE_QUEUE, true);
    }

    @Bean Queue ordersProcessedDurableQueue() {
        return new Queue(PROCESSED_ORDERS_DURABLE_QUEUE, true);
    }

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(DIRECT_EXCHANGE);
    }

    @Bean
    public Binding productsBinding () {
        return BindingBuilder.bind(ordersProcessedDurableQueue()).to(directExchange()).with("processedOrders");
    }

    @Bean
    public Jackson2JsonMessageConverter producerMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerMessageConverter());
        return rabbitTemplate;
    }
}