package com.messaging.rabbit.mq1.messaging.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.messaging.rabbit.mq1.messaging.config.RabbitMQConfig;
import com.messaging.rabbit.mq1.messaging.models.Order;
import com.messaging.rabbit.mq1.messaging.models.Product;
import com.messaging.rabbit.mq1.messaging.models.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
class ProcessedOrderService implements ProcessedOrderServiceIF {
    private final Logger log = LoggerFactory.getLogger(ProcessedOrderService.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Value("${routing.key.for.sending.products}")
    private String routingKey;

    @Value("${routing.key.to.use.for.receiving.processed.order}")
    private String routingKeyWhereToReceive;

    @Value("${exchange.to.use.for.receiving.processed.order}")
    private String exchangeWhereToReceive;

    @Value("${exchange.type.for.sending.products}")
    private String exchangeTypeForSendingProducts;

    @Override
    public void receiveOrderProcessed(final Order order) {
//        log.info("Order ID processed: " + order.getId() + " Status: " + order.getStatus() + " Discount: " + order.getDiscount());
    }

    @Override
    @Scheduled(fixedDelay = 60000)
    public void sendOrder() throws JsonProcessingException {
        long start = System.currentTimeMillis();
        List<Product> products = Arrays.asList(new Product("Socks"), new Product("Socks"), new Product("Socks"), new Product("Socks"),
                new Product("Socks"), new Product("Socks"), new Product("Socks"), new Product("Socks"), new Product("Socks"), new Product("Socks"),
                new Product("Socks"), new Product("Socks"), new Product("Socks"), new Product("Socks"), new Product("Socks"), new Product("Socks"),
                new Product("Socks"), new Product("Socks"), new Product("Socks"));
        Order order = new Order(routingKeyWhereToReceive, exchangeWhereToReceive, products, 0, Status.OPEN);
        for (int i = 0; i < 100000 ; i++ ) {

//            System.out.println(new ObjectMapper().writeValueAsString(order));
            rabbitTemplate.convertAndSend(exchangeTypeForSendingProducts, routingKey, order);
//            log.info("Order sent: " + order.getId() + " Status: " + order.getStatus());

        }
        long end = System.currentTimeMillis();
        log.info("" + TimeUnit.MILLISECONDS.toSeconds(end - start));
    }
}
